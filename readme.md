# INTRODUCRTION
marathon-mania is a SpringBoot application. It implements a POST endpoint to persist a user in DB and upload user's image to S3.

# USAGE
To run the project in development environment clone the project and run `main()` method in `MarathonManiaApplication` class. 
The application will start on port `8080`. 

## END POINTS
The app implements one GET endpoint to easily test application status. A POST endpoint is implemented to create a user.

### GET /greetings/{name}
Returns Hello {name}.

#### EXAMPLE
`http://localhost:8080/greetings/Java` should return *Hello Java*

### POST /user
Creates a new user. To send request using Postman:
1. Select `POST` operation and provide `http://localhost:8080/user` as url
2. Choose `Body` option and choose `form-data`
3. Define following keys and provide data<br>
   1. `name` Optional, Text. User name.
   2. `email` Required, Text. User's email address. Duplicate email addresses are not allowed in the app.
   3. `pwd` Required, Text. User password.
   4. `image` Optional, Image file. User's image file.
4. Choose `Headers` option and make use NO header is selected. 
5. Send the request. If successful, the app will return 201 - Created response code. Otherwise appropriate response code and error message will be returned.

### NOTES
1. To verify the result of POST request go to DB and checkout `USERS` table. It should contain a row for newly created user.
2. `image_path` column contains public url of the uploaded image. Paste it to any browser and the stored image should be displayed.   

   