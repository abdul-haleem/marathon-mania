package com.sensini.marathonmania;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author abdul
 * SpringBoot application entry point.
 */
@SpringBootApplication
public class MarathonManiaApplication {
	private static final Logger logger = LoggerFactory.getLogger(MarathonManiaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(MarathonManiaApplication.class, args);
		logger.info("************** Application Started **************");
	}
}
