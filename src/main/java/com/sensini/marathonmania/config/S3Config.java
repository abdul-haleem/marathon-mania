package com.sensini.marathonmania.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author abdul
 * This class configures AWS S3 client.
 */
@Configuration
public class S3Config {

    @Value("${aws.s3.access.id}")
    private String awsS3AccessId;

    @Value("${aws.s3.secret.key}")
    private String awsS3SecretKey;

    @Value("${aws.s3.bucket.name}")
    private String awsS3BucketName;

    @Value("${aws.s3.region}")
    private String awsS3Region;

    /**
     * Configures AWS S3 client.
     * @return AmazonS3
     */
    @Bean
    public AmazonS3 amazonS3() {
        BasicAWSCredentials credentials = new BasicAWSCredentials(awsS3AccessId, awsS3SecretKey);

        return AmazonS3ClientBuilder.standard()
                .withRegion(Regions.fromName(awsS3Region))
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
    }
}
