package com.sensini.marathonmania.controller;

import com.sensini.marathonmania.model.SinsiniUser;
import com.sensini.marathonmania.service.SinsiniService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author abdul
 * Application main controller.
 */
@RestController
@RequestMapping("/")
public class AppController {

    private static final Logger logger = LoggerFactory.getLogger(AppController.class);

    @Autowired
    private SinsiniService service;

    /**
     * A GET method to verify app is working fine.
     * @param name
     * @return String - A greetings message.
     */
    @GetMapping(path = "greetings/{name}")
    public String greetings(@PathVariable String name) {
        logger.info("A new get request received. name: " + name);
        return "Hello " + name;
    }


    /**
     * POST method to create a user and upload provided image to AWS S3 bucket.<br>
     * To test from Postman select form-data option and provide following parameters.<br>
     * Do not provide content-type header.
     * @param image Optional. Picture file of the user. File type is not checked.
     * @param name Optional. Name of the user.
     * @param email Required. Email id of the user. Only one user with an email id can be created.
     * @param password Required. Password of the user.
     * @return String. Returns empty string with 201 response code if executed successfully.
     */
    @PostMapping(path = "user")
    public ResponseEntity<String> createUser(
            @RequestParam(name = "image", required = false) MultipartFile image,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "email") String email,
            @RequestParam(name = "password") String password) {

            logger.info("A request received to create a new user for email id: " + email);
            SinsiniUser user = SinsiniUser.builder()
                    .image(image)
                    .name(name)
                    .email(email)
                    .password(password)
                    .build();

       service.createUser(user);
       logger.info("User is created.");
       return ResponseEntity.status(HttpStatus.CREATED).build();
    }


}
