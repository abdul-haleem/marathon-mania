package com.sensini.marathonmania.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author abdul
 * Class representing a user to be created.
 */
@Data
@Builder
public class SinsiniUser {
    private Integer id;
    private String name;
    private String email;
    private String password;
    private MultipartFile image;
    private String imageUrl;
}
