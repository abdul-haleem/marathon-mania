package com.sensini.marathonmania.exception.handler;

import com.sensini.marathonmania.exception.FileProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author abdul
 * File processing exceptions handler.
 */
@ControllerAdvice
public class FileProcessingExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(FileProcessingExceptionHandler.class);

    /**
     * Handles file processing exceptions.
     * @param e FileProcessingException
     * @return ResponseEntity
     */
    @ExceptionHandler(FileProcessingException.class)
    public ResponseEntity<String> handleFileProcessingException(FileProcessingException e) {
        logger.error("Error during file processing", e);
        return ResponseEntity
                .status(e.isInternal() ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.BAD_REQUEST)
                .body(e.getSpecificReason());
    }
}
