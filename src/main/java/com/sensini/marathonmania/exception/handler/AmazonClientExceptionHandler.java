package com.sensini.marathonmania.exception.handler;

import com.amazonaws.AmazonClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author abdul
 * S3 exceptions handler.
 */
@ControllerAdvice
public class AmazonClientExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(AmazonClientExceptionHandler.class);

    /**
     * Handles all exceptions thrown by AWS S3 client.
     * @param e AmazonClientException
     * @return ResponseEntity
     */
    @ExceptionHandler(AmazonClientException.class)
    public ResponseEntity<String> handleAmazonClientException(AmazonClientException e) {
        logger.error("Error while accessing S3 bucket", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Unable to access s3 bucket");
    }
}
