package com.sensini.marathonmania.exception;

import lombok.Data;

/**
 * @author abdul
 * A custom exception to handle file processing exceptions.
 */
@Data
public class FileProcessingException extends RuntimeException {

    private String specificReason;
    private boolean internal;


    public FileProcessingException(Throwable cause, String specificReason, boolean isInternal) {
        super(specificReason, cause);
        this.specificReason = specificReason;
        this.internal = isInternal;
    }
}
