package com.sensini.marathonmania.dao;

import com.sensini.marathonmania.entity.SinsiniUserEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * @author abdul
 * Spring data-jpa used for DAO layer.
 */
public interface SinsiniRepository extends CrudRepository<SinsiniUserEntity, Integer>{

}
