package com.sensini.marathonmania.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

/**
 * @author abdul
 * Java Entity mapped to database table.
 */
@Data
@Builder
@Entity
@Table(name = "users")
public class SinsiniUserEntity {

    @Id
    @Column(name = "id")
    @Basic(optional = false)
    private int id;

    @Column(name = "name")
    @Basic(optional = false)
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    @Basic(optional = false)
    private String password;

    @Column(name = "image_path")
    private String imageUrl;
}
