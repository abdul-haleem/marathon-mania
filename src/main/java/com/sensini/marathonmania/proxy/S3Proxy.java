package com.sensini.marathonmania.proxy;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author abdul
 * Interface providing methods to talk to S3.
 */
public interface S3Proxy {

    /**
     * Upload file to S3.
     * @param file MultipartFile
     * @return String. S3 bucket key representing uploaded file.
     */
    String uploadFile(MultipartFile file);

    /**
     * Deletes a file from S3 bucket.
     * @param key String. Key of the file to be deleted.
     */
    void deleteFile(String key);

    /**
     * Builds and returns file's complete url which is publicly available.
     * @param key String. Key of the file.
     * @return String. Publicly accessible URL.
     */
    String getFileUrl(String key);
}
