package com.sensini.marathonmania.proxy;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.sensini.marathonmania.exception.FileProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * {@inheritDoc}
 */
@Service
public class S3ProxyImpl implements S3Proxy {

    private static final Logger logger = LoggerFactory.getLogger(S3ProxyImpl.class);

    @Autowired
    private AmazonS3 amazonS3;

    @Value("${aws.s3.bucket.name}")
    private String awsS3BucketName;

    @Value("${aws.s3.region}")
    private String awsS3Region;

    /**
     * {@inheritDoc}
     */
    @Override
    public String uploadFile(MultipartFile multipartFile) {
        File file = multipartToFile(multipartFile);
        String key = generateKey(multipartFile.getOriginalFilename());
        logger.info("File key generated: " + key);
        amazonS3.putObject(
                new PutObjectRequest(awsS3BucketName, key, file)
                        .withCannedAcl(CannedAccessControlList.PublicRead)
        );
        file.delete();

        return key;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFile(String key) {
        amazonS3.deleteObject(awsS3BucketName, key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getFileUrl(String key) {
        return new StringBuilder("https://s3.")
                .append(awsS3Region)
                .append(".amazonaws.com")
                .append(File.separator)
                .append(awsS3BucketName)
                .append(File.separator)
                .append(key)
                .toString();
    }

    private File multipartToFile(MultipartFile multipartFile) {
        try {
            File file = new File(multipartFile.getOriginalFilename());
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(multipartFile.getBytes());
            outputStream.close();
            logger.debug("Multipart file converted to regular java file");
            return file;
        } catch (IOException e) {
            throw new FileProcessingException(e, "Bad image file", false);
        }
    }

    private String generateKey(String filename) {
        return System.currentTimeMillis() + "_" + filename;
    }
}
