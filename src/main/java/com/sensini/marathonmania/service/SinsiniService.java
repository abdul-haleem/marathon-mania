package com.sensini.marathonmania.service;

import com.sensini.marathonmania.model.SinsiniUser;

/**
 * @author abdul
 * Provides method to create a user.
 */
public interface SinsiniService {

    /**
     * Creates new user in DB and uploads provided image to S3.
     * @param user
     */
    void createUser(SinsiniUser user);
}
