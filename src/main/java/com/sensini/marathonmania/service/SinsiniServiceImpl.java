package com.sensini.marathonmania.service;

import com.sensini.marathonmania.dao.SinsiniRepository;
import com.sensini.marathonmania.entity.SinsiniUserEntity;
import com.sensini.marathonmania.model.SinsiniUser;
import com.sensini.marathonmania.proxy.S3Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

/**
 * {@inheritDoc}
 */
@Service
public class SinsiniServiceImpl implements SinsiniService {

    private static final Logger logger = LoggerFactory.getLogger(SinsiniServiceImpl.class);

    @Autowired
    private SinsiniRepository repository;

    @Autowired
    private S3Proxy s3Proxy;

    /**
     *{@inheritDoc}
     */
    @Override
    public void createUser(SinsiniUser user) {
        String key = null;

        if (user.getImage() != null) {
            key = s3Proxy.uploadFile(user.getImage());
            String imageUrl = s3Proxy.getFileUrl(key);
            user.setImageUrl(imageUrl);
            logger.info("File uploaded to: " + imageUrl);
        }

        try {
            repository.save(userModelToEntity(user));
            logger.info("User information persisted to DB");
        } catch (DataAccessException e) {
            if (key != null) {
                s3Proxy.deleteFile(key);
                logger.warn("File deleted from S3 due to above exceptions");
            }
            throw e;
        }
    }

    private SinsiniUserEntity userModelToEntity(SinsiniUser user) {
        SinsiniUserEntity entity = SinsiniUserEntity.builder()
                .name(user.getName())
                .email(user.getEmail())
                .password(user.getPassword())
                .imageUrl(user.getImageUrl())
                .build();

        return entity;
    }
}
