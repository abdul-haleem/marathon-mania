package com.sensini.marathonmania.dao;


import com.sensini.marathonmania.entity.SinsiniUserEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SinsiniRepositoryTest {

    @Autowired
    private TestEntityManager em;

    @Autowired
    private SinsiniRepository repository;

    @Before
    public void setup() {
        repository.deleteAll();
    }

    @Test
    public void testInsertAll() {
        //Insert data in DB
        SinsiniUserEntity entity = SinsiniUserEntity.builder()
                .name("Adam Yellow")
                .email("adam@yellow.com")
                .password("mySecretPwd")
                .imageUrl("http://localhost:8080/test_image.jpg")
                .build();

        repository.save(entity);

        // Fetch it back
        SinsiniUserEntity entityFromDB = repository.findAll().iterator().next();

        //Verify
        assertNotNull(entityFromDB);
        assertEquals(entity.getEmail(),entityFromDB.getEmail());

    }
}