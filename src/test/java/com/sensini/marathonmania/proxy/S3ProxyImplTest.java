package com.sensini.marathonmania.proxy;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class S3ProxyImplTest {

    @TestConfiguration
    static class TestConfig {
        @Bean
        public S3Proxy s3Proxy() {
            return new S3ProxyImpl();
        }
    }


    @MockBean
    private AmazonS3 amazonS3;

    @Autowired
    S3Proxy s3Proxy;


    @Test
    public void uploadFile() {
        when(amazonS3.putObject(any(PutObjectRequest.class))).thenReturn(null);
        MockMultipartFile multipartFile = new MockMultipartFile(
                "test_file"
                ,"test_file.jpg"
                ,"image/jpeg"
                , "test image content".getBytes()
        );

        String key = s3Proxy.uploadFile(multipartFile);

        assertNotNull(key);
        verify(amazonS3, times(1)).putObject(any(PutObjectRequest.class));

    }
}