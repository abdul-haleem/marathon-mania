package com.sensini.marathonmania.service;

import com.sensini.marathonmania.dao.SinsiniRepository;
import com.sensini.marathonmania.entity.SinsiniUserEntity;
import com.sensini.marathonmania.model.SinsiniUser;
import com.sensini.marathonmania.proxy.S3Proxy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class SinsiniServiceImplTest {

    @TestConfiguration
    static class TestConfig {

        @Bean
        public SinsiniService sinsiniService() {
            return new SinsiniServiceImpl();
        }

    }

    @MockBean
    private SinsiniRepository repository;

    @MockBean
    private S3Proxy s3Proxy;

    @Autowired
    private SinsiniService service;

    @Test
    public void createUserNoImage() {
        SinsiniUser user = SinsiniUser.builder()
                .name("Adam Yellow")
                .email("adam@yellow.com")
                .password("mySecretPwd")
                .build();

        service.createUser(user);

        verify(s3Proxy, times(0)).uploadFile(any(MultipartFile.class));
        verify(repository, times(1)).save(any(SinsiniUserEntity.class));

    }

    @Test
    public void createUser() {
        MockMultipartFile multipartFile = new MockMultipartFile(
                "test_file"
                ,"test_file.jpg"
                ,"image/jpeg"
                , "test image content".getBytes()
        );

        when(s3Proxy.uploadFile(multipartFile)).thenReturn("test_key");

        SinsiniUser user = SinsiniUser.builder()
                .name("Adam Yellow")
                .email("adam@yellow.com")
                .password("mySecretPwd")
                .image(multipartFile)
                .build();

        service.createUser(user);

        verify(s3Proxy, times(1)).uploadFile(multipartFile);
        verify(repository, times(1)).save(any(SinsiniUserEntity.class));

    }
}